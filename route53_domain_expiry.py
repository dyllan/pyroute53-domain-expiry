#!/usr/bin/env python

import boto3, datetime
from sys import argv

script, expiry_days_limit, max_items_per_page = argv

"""
AWS boto3 module to get a list of route53domains.
Save list of domains in an object called response.
Use NextPageMarker for pagination. Get the current date and time.
"""
client = boto3.client("route53domains", region_name = "us-east-1")
response = client.list_domains(MaxItems = int(max_items_per_page))
next_page = response["NextPageMarker"]
date_now = datetime.datetime.now().date()


def domain_expiry(domain_key):
    """
    Get DomainName, Expiry keys and their values.
    """
    domain_name = domain_key["DomainName"]
    expiry_date = domain_key["Expiry"].replace(tzinfo=None).date()
    expiry_days = str(expiry_date - date_now)
    expiry_days = int(expiry_days.split(" ")[0])
    if expiry_days <= int(expiry_days_limit):
        print(f"{domain_name}, expires in {expiry_days} days")


def domain_loop():
    """
    Loop through response and call domain_expiry method.
    """
    for domain_key in response["Domains"]:
        domain_expiry(domain_key)


domain_loop()

while next_page:
    """
    If next_page is True call our domain_expiry method until False.
    """
    response = client.list_domains(Marker = next_page, MaxItems = int(max_items_per_page))
    try:
        next_page = response["NextPageMarker"]
    except:
        next_page = False
    domain_loop()

