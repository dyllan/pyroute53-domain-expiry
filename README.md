# pyRoute53 Domain Expiry

Quick script to help you get a list of your Route53 domains expiring in under **x** days.

Makes calls to AWS Python API and returns a full set of values utilizing pagination.


**Dependencies:**
* python3.x
* aws cli
* boto3

[Boto3 Install](https://boto3.amazonaws.com/v1/documentation/api/latest/guide/quickstart.html)
*(Be sure to configure your aws credentials.)*



**Usage:**

`route53_domain_expiry.py days_expiring results_per_page`



**Arguments:**

`days_expiring --> 60 (Will give you all domains expiring in 60 or less days time.)`

`results_per_page --> 50 (Will return 50 results per page, max results per page is 100)`



**Example:**

`$ python route53_domain_expiry.py 60 50`